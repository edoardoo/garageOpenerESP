#include <RCSwitch.h>
#include <ESP8266WiFi.h>          //https://github.com/esp8266/Arduino
#include <ESP8266WebServerSecure.h>
#include <DNSServer.h>
#include <WiFiManager.h>          //https://github.com/tzapu/WiFiManager

//for LED status
#include <Ticker.h>
Ticker ticker;
//ESP8266WebServer serverApi(3000);
BearSSL::ESP8266WebServerSecure serverApi(3000);

bool isConnected = false;
int transmitPin = 0; 
RCSwitch mySwitch = RCSwitch();


static const char serverCert[] PROGMEM = R"EOF(
-----BEGIN CERTIFICATE-----
MIIFXDCCBESgAwIBAgISA61AteKFlQo7Le2gPRJkeL8FMA0GCSqGSIb3DQEBCwUA
MEoxCzAJBgNVBAYTAlVTMRYwFAYDVQQKEw1MZXQncyBFbmNyeXB0MSMwIQYDVQQD
ExpMZXQncyBFbmNyeXB0IEF1dGhvcml0eSBYMzAeFw0xODExMzAyMTE2MzJaFw0x
OTAyMjgyMTE2MzJaMB0xGzAZBgNVBAMTEmJvaXRvLmFzdXNjb21tLmNvbTCCASIw
DQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAKETjB2a1iAO+lheGc8kLkbAcaD1
pYzW82QUAB5YqHm+zJCkrqSe6CwAOR6Js2EEoNwlDIA7m5naXSPGBGS0D0Ex6Dos
EVqLz8slT1i9oXwOACnSTmYOx4631qicif+5RmJLAJds/Vy74VlJa1Z37twZGGvX
C2tPOC/AQYfLK5GRH9ulWhPiG0wQsmVNu4pt5kWABsj+UHl5J2nI2NRQWIn/rG90
TuWisCb7zWO5zyGsQCbcpL0NenoPwTdAaOFiqlzMgit4A4XQES/5XJcReJwmov/j
nNcxKnnkvfIAXkKJx4pnSEFoo5gTf8NCGX+bbKGIbAi0CkvlQ+7cjbbvApUCAwEA
AaOCAmcwggJjMA4GA1UdDwEB/wQEAwIFoDAdBgNVHSUEFjAUBggrBgEFBQcDAQYI
KwYBBQUHAwIwDAYDVR0TAQH/BAIwADAdBgNVHQ4EFgQUEyDQ1NKMAXEHbIZU0bG8
dWCpQa0wHwYDVR0jBBgwFoAUqEpqYwR93brm0Tm3pkVl7/Oo7KEwbwYIKwYBBQUH
AQEEYzBhMC4GCCsGAQUFBzABhiJodHRwOi8vb2NzcC5pbnQteDMubGV0c2VuY3J5
cHQub3JnMC8GCCsGAQUFBzAChiNodHRwOi8vY2VydC5pbnQteDMubGV0c2VuY3J5
cHQub3JnLzAdBgNVHREEFjAUghJib2l0by5hc3VzY29tbS5jb20wTAYDVR0gBEUw
QzAIBgZngQwBAgEwNwYLKwYBBAGC3xMBAQEwKDAmBggrBgEFBQcCARYaaHR0cDov
L2Nwcy5sZXRzZW5jcnlwdC5vcmcwggEEBgorBgEEAdZ5AgQCBIH1BIHyAPAAdgB0
ftqDMa0zEJEhnM4lT0Jwwr/9XkIgCMY3NXnmEHvMVgAAAWdmseMLAAAEAwBHMEUC
IAFxLyux8XLD6xLfEYhG7cgfqUJP04kriBKwquOLfq2QAiEAhjwxp44dwXqOd4NC
qpP6SZkhzCzBBd69n+0pJ/UuEDQAdgBj8tvN6DvMLM8LcoQnV2szpI1hd4+9daY4
scdoVEvYjQAAAWdmseMXAAAEAwBHMEUCIEpsd5+wokjaBniZPZaXDt+tKaFZEAbN
NTQ0fnWvhs2TAiEAnoMB2aCXh/VdezRn9QqNQAgmaZE+nOqQXMiEfQZO52owDQYJ
KoZIhvcNAQELBQADggEBAGuR8NRDZNTLMjGzVIcQB0SjBR6NtAjj0zKZ0V88y6wr
SgcKTryxthNB3WYj7tjTWfXJ+qfirzvqL0rtcwtSwP4xr56YTJntaacCF63sdcUI
7CAyzP2kRy7Lf0RIGgg4nelx6fJYO/0OjuJB/1ZAvGlytz83X7NnVI9fgved70Eh
poqT04TWTuwYfJESqS/T6rEyO2DoqMJ5RcdUixYCzfdeh/797mW4sZP0jKJSG3m9
x7ulDrlqSuERRIYqS3fnX3oWk9a1bwV3gwKk6Ezkqogafk4nuBzr65IqxXErp9HK
FidUQieSjVwUEpWP3mOw22hF98rx0CxPTZm53r6vVYw=
-----END CERTIFICATE-----
)EOF";

static const char serverKey[] PROGMEM =  R"EOF(
-----BEGIN PRIVATE KEY-----
MIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQChE4wdmtYgDvpY
XhnPJC5GwHGg9aWM1vNkFAAeWKh5vsyQpK6knugsADkeibNhBKDcJQyAO5uZ2l0j
xgRktA9BMeg6LBFai8/LJU9YvaF8DgAp0k5mDseOt9aonIn/uUZiSwCXbP1cu+FZ
SWtWd+7cGRhr1wtrTzgvwEGHyyuRkR/bpVoT4htMELJlTbuKbeZFgAbI/lB5eSdp
yNjUUFiJ/6xvdE7lorAm+81juc8hrEAm3KS9DXp6D8E3QGjhYqpczIIreAOF0BEv
+VyXEXicJqL/45zXMSp55L3yAF5CiceKZ0hBaKOYE3/DQhl/m2yhiGwItApL5UPu
3I227wKVAgMBAAECggEABPHwr0f9ouZS0TuyVyWpNgracjKYboZ62muyxTu7kGFH
Lnmf0vX98rvfBQz9MNFPjvKkO5uY31RQ37RIlj7g6QN7Oa34qmr4aigvE25e8swL
PT1zorZ5cew67to/clZhzsq2re9up9YsyOz0Gqd78c3jbgwgEZ9Q/kZMvLqED+f0
Ne/eic3H/OlN80VxbhuojNJSaA0V2BP5Oep9XMVWK3H2HC3LH3Hdy7E/tYKSemKW
s3jsmBw5EkGrFU2Tf701nTsXlAHcUMSsUtrfBQXd6wxWpyIlbCsEvHNtg0ZNs3fa
+quAZ694PkZjiPR2vxc1B0Gwu24PerYBZCgnVAoQgQKBgQDO5tnk+Vp/jwQKfZSW
sBJ933NzgbDplXWXYMoDRmMJwUbznRxZCX7ZU6SFadjV+gGybWdzreqLfA39tixa
aRjOyMq3lu1/cdQ3Nb+mCVYXzGWBdSL+bum2Hso242nfxdamYIO7lt3W60xF93aN
q/TIR+cAZa6X0elT3KWyoUBn6QKBgQDHTNURIyQixo4Stex1Uta+WXgPqx7YvyN1
1qVZ+Xrt/BvEw5ivd8Bv0e+IQuB3QHgHm/yQGbhp13zCG7bROp2pZBOAY3/JeB7U
cGdHnefKjlJ5JeEe4FWXCB/gSaYltHxcY5fVjIfBQZCJyL3Xt2kLWk1yHE3bOKZr
+psvCUpFzQKBgQCVBinDhubsIbKja7huoPtwtlWNyMgINqKwGgLnQPdsu03DglI8
v14iYuCJR6fq11wJjEuLQqVBOmVJbMGdjT1VE8MPYSJHPVHBQnF7whXEpnAmQpyl
7nVfu+Wii0Ji7zigNbwsjYnTlz8k1g0KFeUFrORxoMuUhpks0ix28TZWoQKBgDtY
gp2uxCTi0Q17RKUO/MkDpT+tKetKx0lgmC45nsRc7PvjboTZeOETN2tw1pxMAzHo
3gUe044Yz5qRXBfjh20YUsJJXpTkgvGpYueteocTaDWQ9P8XuATYM327EJFrtBhi
w6SgSxBd8KdGslkY0VvDEQqoqPpdXRkML8eK0aYFAoGBAMSKSQU73JoBCh7aSVGk
W17c4/+WVnpi611bGw3Wi0J/GACgG2YFJVZhpPl6ivx5onNosKxlhfswcgbLBYZn
U1QdNRrTie/itmkEBul2yI9SKpIGP/aae8vWEdtwpdZ9Q0wOtxhnIbBx4xFqm6TG
zftEoxAbGWuE3g1kZMgZhznX
-----END PRIVATE KEY-----
)EOF";



void handleRoot() {
//  digitalWrite(led, 1);
  serverApi.send(200, "text/plain", "Hello from esp8266 over HTTPS!");
//  digitalWrite(led, 0);
}

void handleNotFound(){
//  digitalWrite(led, 1);
  String message = "File Not Found\n\n";
  message += "URI: ";
  message += serverApi.uri();
  message += "\nMethod: ";
  message += (serverApi.method() == HTTP_GET)?"GET":"POST";
  message += "\nArguments: ";
  message += serverApi.args();
  message += "\n";
  for (uint8_t i=0; i<serverApi.args(); i++){
    message += " " + serverApi.argName(i) + ": " + serverApi.arg(i) + "\n";
  }
  serverApi.send(404, "text/plain", message);
//  digitalWrite(led, 0);
}

struct Message{
  int * message;
  int length;
};

void tick(){
  //toggle state
  int state = digitalRead(BUILTIN_LED);  // get the current state of GPIO1 pin
  digitalWrite(BUILTIN_LED, !state);     // set pin to the opposite state
}


void customDelay(unsigned long time) {
    unsigned long end_time = micros() + time;
    while(micros() < end_time);
}

void setStateWithDelay(int pin, int state,int delayTime){
//  Serial.print(delayTime);
//  Serial.print(",");

  if(state==1)
    digitalWrite(pin,HIGH);
  else
    digitalWrite(pin,LOW);
    
  customDelay(delayTime);
}

struct Message parseMessage ( String message ){
  int i,parsedMessageLength ;
  int j = 0;
  int * parsedMessage;
  char * parsedChars;
  struct Message responseMessage;
  for (i=0, parsedMessageLength=0; message[i]; i++)
    parsedMessageLength += (message[i] == ',');
  parsedMessageLength++;

  Serial.println(parsedMessageLength);
  
  parsedMessage = (int *) malloc( sizeof(int *) * parsedMessageLength);

  parsedChars =  strtok(  (char *) message.c_str() , ",");


  while(  parsedChars ){
  
      parsedMessage[j] = (int )atoi(parsedChars) ;

      parsedChars =  strtok(  NULL , ",");
      j++;
  }

  responseMessage.message = parsedMessage;
  responseMessage.length = parsedMessageLength;
  return responseMessage;
  
  
}

void configModeCallback (WiFiManager *myWiFiManager) {
  Serial.println("Entered config mode");
  Serial.println(WiFi.softAPIP());
  //if you used auto generated SSID, print it
  Serial.println(myWiFiManager->getConfigPortalSSID());
  //entered config mode, make led toggle faster
  ticker.attach(0.2, tick);
}

void openGate(){
//10972,320,630,320,320,630,320,630,630,320,320,630,320,630,320,630,320,630,630,320,630,320,630,320,630,320,630
  setStateWithDelay(transmitPin,0,10972);
  setStateWithDelay(transmitPin,1,320);
  setStateWithDelay(transmitPin,0,630);
  setStateWithDelay(transmitPin,1,320);
  setStateWithDelay(transmitPin,0,320);
  setStateWithDelay(transmitPin,1,630);
  setStateWithDelay(transmitPin,0,320);
  setStateWithDelay(transmitPin,1,630);
  setStateWithDelay(transmitPin,0,630);
  setStateWithDelay(transmitPin,1,320);
  setStateWithDelay(transmitPin,0,320);
  setStateWithDelay(transmitPin,1,630);
  setStateWithDelay(transmitPin,0,320);
  setStateWithDelay(transmitPin,1,630);
  setStateWithDelay(transmitPin,0,320);
  setStateWithDelay(transmitPin,1,630);
  setStateWithDelay(transmitPin,0,320);
  setStateWithDelay(transmitPin,1,630);
  setStateWithDelay(transmitPin,0,630);
  setStateWithDelay(transmitPin,1,320);
  setStateWithDelay(transmitPin,0,630);
  setStateWithDelay(transmitPin,1,320);
  setStateWithDelay(transmitPin,0,630);
  setStateWithDelay(transmitPin,1,320);
  setStateWithDelay(transmitPin,0,630);
  setStateWithDelay(transmitPin,1,320);
  setStateWithDelay(transmitPin,0,630);
  
   
   digitalWrite(transmitPin,LOW);
}
void setup() {
  // put your setup code here, to run once:
  Serial.begin(115200);
  pinMode(transmitPin, OUTPUT);
  digitalWrite(transmitPin,LOW);
  mySwitch.enableTransmit(0);
  
  //set led pin as output
  pinMode(BUILTIN_LED, OUTPUT);
  // start ticker with 0.5 because we start in AP mode and try to connect
  ticker.attach(0.6, tick);

  //WiFiManager
  //Local intialization. Once its business is done, there is no need to keep it around
  WiFiManager wifiManager;
  //reset settings - for testing
 // wifiManager.resetSettings();

  //set callback that gets called when connecting to previous WiFi fails, and enters Access Point mode
  wifiManager.setAPCallback(configModeCallback);


  //fetches ssid and pass and tries to connect
  //if it does not connect it starts an access point with the specified name
  //here  "AutoConnectAP"
  //and goes into a blocking loop awaiting configuration
  if (!wifiManager.autoConnect("Homie")) {
    Serial.println("failed to connect and hit timeout");
    //reset and try again, or maybe put it to deep sleep
    delay(3000);
    digitalWrite(transmitPin,HIGH);
    ESP.reset();
    digitalWrite(transmitPin,LOW);
    delay(1000);
  }

  //if you get here you have connected to the WiFi
  isConnected = true;
  Serial.println("WiFi Connected.");
  serverApi.setRSACert(new BearSSLX509List(serverCert), new BearSSLPrivateKey(serverKey));


  serverApi.on("/open", [](){
    Serial.println("Opening gate.");
    
    serverApi.send(200, "text/plain", "Opening the gate");
    for( int i=0; i<20; i++){
      openGate();
    }
    
    
  });

  
  serverApi.on("/send", [](){
    Serial.println("received SEND request");
    Serial.print(serverApi.arg("m"));

    serverApi.send(200, "text/plain", "Message Sent");
    struct Message message  = parseMessage( serverApi.arg("m") );
    mySwitch.setPulseLength( 184 );
    
    for( int i=0; i<10; i++){
      sendMessage( message );
    }
    
    
  });

  serverApi.on("/sendDecimal", [](){
    Serial.println("received SEND request");
    Serial.print(serverApi.arg("m"));
    serverApi.send(200, "text/plain", "Message Sent");
    struct Message message  = parseMessage( serverApi.arg("m") );
    mySwitch.setPulseLength( 189 );
    mySwitch.send( message.message[0] , 24);
    delay(50);  
  
  });

  serverApi.onNotFound(handleNotFound);


  serverApi.begin();
  ticker.detach();
  //keep LED on
  digitalWrite(BUILTIN_LED, LOW);
}



void loop() {
  // put your main code here, to run repeatedly:
  if( isConnected ){
    serverApi.handleClient();
  }
}

void sendMessage( struct Message message){
//  Serial.println("Got Message to send.");

  int k;
  int bitStatus = 0;


  for ( k = 0; k < message.length; k++){

      bitStatus = ( k%2 == 0) ? 0 : 1;
      setStateWithDelay(transmitPin,bitStatus,message.message[k]);
  }

  digitalWrite(transmitPin,LOW);
  Serial.println("\n\r------------------------");


}
