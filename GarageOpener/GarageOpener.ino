#include <RCSwitch.h>
#include <ESP8266WiFi.h>          //https://github.com/esp8266/Arduino
#include <ESP8266WebServerSecure.h>
#include <DNSServer.h>
#include <WiFiManager.h>          //https://github.com/tzapu/WiFiManager

//for LED status
#include <Ticker.h>
Ticker ticker;
ESP8266WebServer serverApi(3000);
bool isConnected = false;
int transmitPin = 0; 
RCSwitch mySwitch = RCSwitch();




struct Message{
  int * message;
  int length;
};

void tick()
{
  //toggle state
  int state = digitalRead(BUILTIN_LED);  // get the current state of GPIO1 pin
  digitalWrite(BUILTIN_LED, !state);     // set pin to the opposite state
}


void customDelay(unsigned long time) {
    unsigned long end_time = micros() + time;
    while(micros() < end_time);
}

void setStateWithDelay(int pin, int state,int delayTime)
{
    Serial.print(delayTime);
    Serial.print(",");


  if(state==1)
    digitalWrite(pin,HIGH);
  else
    digitalWrite(pin,LOW);
    
  customDelay(delayTime);
}

struct Message parseMessage ( String message ){
  int i,parsedMessageLength ;
  int j = 0;
  int * parsedMessage;
  char * parsedChars;
  struct Message responseMessage;
  for (i=0, parsedMessageLength=0; message[i]; i++)
    parsedMessageLength += (message[i] == ',');
  parsedMessageLength++;

  Serial.println(parsedMessageLength);
  
  parsedMessage = (int *) malloc( sizeof(int *) * parsedMessageLength);

  parsedChars =  strtok(  (char *) message.c_str() , ",");


  while(  parsedChars ){
  
      parsedMessage[j] = (int )atoi(parsedChars) ;

      parsedChars =  strtok(  NULL , ",");
      j++;
  }

  responseMessage.message = parsedMessage;
  responseMessage.length = parsedMessageLength;
  return responseMessage;
  
  
}

//gets called when WiFiManager enters configuration mode
void configModeCallback (WiFiManager *myWiFiManager) {
  Serial.println("Entered config mode");
  Serial.println(WiFi.softAPIP());
  //if you used auto generated SSID, print it
  Serial.println(myWiFiManager->getConfigPortalSSID());
  //entered config mode, make led toggle faster
  ticker.attach(0.2, tick);
}
void openGate(){
//    10972,320,630,320,320,630,320,630,630,320,320,630,320,630,320,630,320,630,630,320,630,320,630,320,630,320,630
  setStateWithDelay(transmitPin,0,10972);
  setStateWithDelay(transmitPin,1,320);
  setStateWithDelay(transmitPin,0,630);
  setStateWithDelay(transmitPin,1,320);
  setStateWithDelay(transmitPin,0,320);
  setStateWithDelay(transmitPin,1,630);
  setStateWithDelay(transmitPin,0,320);
  setStateWithDelay(transmitPin,1,630);
  setStateWithDelay(transmitPin,0,630);
  setStateWithDelay(transmitPin,1,320);
  setStateWithDelay(transmitPin,0,320);
  setStateWithDelay(transmitPin,1,630);
  setStateWithDelay(transmitPin,0,320);
  setStateWithDelay(transmitPin,1,630);
  setStateWithDelay(transmitPin,0,320);
  setStateWithDelay(transmitPin,1,630);
  setStateWithDelay(transmitPin,0,320);
  setStateWithDelay(transmitPin,1,630);
  setStateWithDelay(transmitPin,0,630);
  setStateWithDelay(transmitPin,1,320);
  setStateWithDelay(transmitPin,0,630);
  setStateWithDelay(transmitPin,1,320);
  setStateWithDelay(transmitPin,0,630);
  setStateWithDelay(transmitPin,1,320);
  setStateWithDelay(transmitPin,0,630);
  setStateWithDelay(transmitPin,1,320);
  setStateWithDelay(transmitPin,0,630);
  
   
   digitalWrite(transmitPin,LOW);
}
void setup() {
  // put your setup code here, to run once:
  Serial.begin(115200);
  pinMode(transmitPin, OUTPUT);
  digitalWrite(transmitPin,LOW);
  mySwitch.enableTransmit(0);
  
  //set led pin as output
  pinMode(BUILTIN_LED, OUTPUT);
  // start ticker with 0.5 because we start in AP mode and try to connect
  ticker.attach(0.6, tick);

  //WiFiManager
  //Local intialization. Once its business is done, there is no need to keep it around
  WiFiManager wifiManager;
  //reset settings - for testing
 // wifiManager.resetSettings();

  //set callback that gets called when connecting to previous WiFi fails, and enters Access Point mode
  wifiManager.setAPCallback(configModeCallback);


  //fetches ssid and pass and tries to connect
  //if it does not connect it starts an access point with the specified name
  //here  "AutoConnectAP"
  //and goes into a blocking loop awaiting configuration
  if (!wifiManager.autoConnect("Homie")) {
    Serial.println("failed to connect and hit timeout");
    //reset and try again, or maybe put it to deep sleep
    delay(3000);
    digitalWrite(transmitPin,HIGH);
    ESP.reset();
    digitalWrite(transmitPin,LOW);
    delay(1000);
  }

  //if you get here you have connected to the WiFi
  isConnected = true;
  Serial.println("WiFi Connected.");

  serverApi.on("/open", [](){
    Serial.println("Opening gate.");
    
    serverApi.send(200, "text/plain", "Opening the gate");
    for( int i=0; i<20; i++){
      openGate();
    }
    
    
  });

  
  serverApi.on("/send", [](){
    Serial.println("received SEND request");
    Serial.print(serverApi.arg("m"));

    serverApi.send(200, "text/plain", "Message Sent");
    struct Message message  = parseMessage( serverApi.arg("m") );
    mySwitch.setPulseLength( 184 );
    
    for( int i=0; i<10; i++){
      sendMessage( message );
    }
    
    
  });

  serverApi.on("/sendDecimal", [](){
    Serial.println("received SEND request");
    Serial.print(serverApi.arg("m"));
    serverApi.send(200, "text/plain", "Message Sent");
    struct Message message  = parseMessage( serverApi.arg("m") );
    mySwitch.setPulseLength( 189 );
    mySwitch.send( message.message[0] , 24);
//    mySwitch.send( message.message[0] , 24);

    delay(50);  
  
  });
  
  serverApi.begin();
  ticker.detach();
  //keep LED on
  digitalWrite(BUILTIN_LED, LOW);
}



void loop() {
  // put your main code here, to run repeatedly:
  if( isConnected ){
    serverApi.handleClient();
  }
}

void sendMessage( struct Message message){
//  Serial.println("Got Message to send.");

  int k;
  int bitStatus = 0;


  for ( k = 0; k < message.length; k++){

      bitStatus = ( k%2 == 0) ? 0 : 1;
      setStateWithDelay(transmitPin,bitStatus,message.message[k]);
  }

  digitalWrite(transmitPin,LOW);
  Serial.println("\n\r------------------------");


}
